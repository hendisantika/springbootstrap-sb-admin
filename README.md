# springbootstrap-sb-admin

A demonstration of using [Spring Boot](https://spring.io/projects/spring-boot) (Thymeleaf and Security starters) to implement a secured "admin" interface provided by [StartBootstrap's SB Admin 2 theme](https://startbootstrap.com/template-overviews/sb-admin-2/) via [WebJars](https://www.webjars.org/).

### Usage

#### Run the application

**Maven**

`mvn spring-boot:run`

### Within your IDE

    Main class is com.hendisantika.sbadmin2.SpringBootSbAdminDemoApplication

### Open your browser

```
    URL: http://localhost:8080
    Username: admin
    Password: password
```

### Screenshot

Login Page

![Login Page](img/login.png "Login Page")

Dashboard Page

![Dashboard Page](img/dashboard.png "Dashboard Page")
