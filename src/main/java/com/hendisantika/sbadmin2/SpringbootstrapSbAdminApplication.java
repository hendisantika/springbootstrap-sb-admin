package com.hendisantika.sbadmin2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootstrapSbAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootstrapSbAdminApplication.class, args);
    }
}
